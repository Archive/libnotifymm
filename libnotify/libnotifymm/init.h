// -*- c++ -*-
#ifndef _LIBNOTIFYMM_INIT_H
#define _LIBNOTIFYMM_INIT_H
/* $Id: init.h 497 2003-10-12 09:50:16Z murrayc $ */

/* init.h
 *
 * Copyright 2001      Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glibmm.h>


namespace Notify
{
/** @defgroup Init Initialisation Functions
 *
 * This section describes a number of functions for initialisation
 * of libnotifymm
 */
	
/** Initializes the notifications library.
 * @ingroup Init
 * @param app_name The application name.
 *
 * @return <tt>true</tt> if the library initialized properly and a connection to a
 *         notification server was made.
 */	
bool init(const Glib::ustring& app_name);

/** Uninitializes the notifications library.
 * @ingroup Init
 * This will be automatically called on exit unless previously called.
 */
void uninit();
	
/** Returns whether or not the notification library is initialized.
 * @ingroup Init
 *
 * @return <tt>true</tt> if the library is initialized, or <tt>false</tt>.
 */
bool is_initted();
	
} //namespace Notify

#endif //_LIBNOTIFYMM_INIT_H

