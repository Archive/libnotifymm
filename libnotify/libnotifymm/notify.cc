// -*- c++ -*-
/* $Id: init.cc 622 2004-03-29 17:59:17Z murrayc $ */

/* init.cc
 *
 * Copyright 2001      Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libnotify/notify.h>
#include <glibmm.h>

namespace Notify
{

Glib::ustring get_app_name()
{
  return notify_get_app_name();    
}

void set_app_name(const Glib::ustring& app_name)
{
  notify_set_app_name(app_name.c_str());
}
  
Glib::ListHandle<Glib::ustring> get_server_caps()
{
  return Glib::ListHandle<Glib::ustring>(notify_get_server_caps(), Glib::OWNERSHIP_SHALLOW);
}

bool get_server_info (Glib::ustring& ret_name, Glib::ustring& ret_vendor, Glib::ustring& ret_version, Glib::ustring& ret_spec_version)
{
  gchar* name = 0;
  gchar* vendor = 0;
  gchar* version = 0;
  gchar* spec_version = 0;
  
  if (!notify_get_server_info (&name, &vendor, &version, &spec_version))
    return false;
  
  ret_name = Glib::convert_return_gchar_ptr_to_ustring(name);
  ret_vendor = Glib::convert_return_gchar_ptr_to_ustring(vendor);
  ret_version = Glib::convert_return_gchar_ptr_to_ustring(version);
  ret_spec_version = Glib::convert_return_gchar_ptr_to_ustring(spec_version);
  
  return true;
}


} //namespace Notify

