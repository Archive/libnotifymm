/*
 * Copyright 2001      Free Software Foundation
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <glibmm/ustring.h>
#include <glibmm/listhandle.h>

namespace Notify
{

/** @defgroup InfoUtils Info Utility Functions
 *
 * This section describes a number of utility functions to get
 * information about the environment
 */

/** Returns the application name
 * @ingroup InfoUtils
 * @return application name
 */
Glib::ustring get_app_name();

/** Sets the application name.
 * @ingroup InfoUtils
 * @param app_name Application name
 */
void set_app_name(const Glib::ustring& app_name);

/** Get a list of capability strings
 * @ingroup InfoUtils
 * @return a list of capabbility strings
 */
Glib::ListHandle<Glib::ustring> get_server_caps();

/** Returns the server notification information.
 * @ingroup InfoUtils
 * @param ret_name         The returned product name of the server.
 * @param ret_vendor       The returned vendor.
 * @param ret_version      The returned server version.
 * @param ret_spec_version The returned specification version supported.
 * @return <tt>true</tt> if the call succeded, or <tt>false</tt> if there were errors.
 */
bool get_server_info (Glib::ustring& ret_name, Glib::ustring& ret_vendor, Glib::ustring& ret_version, Glib::ustring& ret_spec_version);

} //namespace Notify
