#!/bin/bash

# libnotifymm/tools/generate_defs_and_docs.sh

# This script must be executed from directory libnotifymm/tools.

# Assumed directory structure:
#   glibmm/tools/defs_gen/docextract_to_xml.py
#   glibmm/tools/defs_gen/h2def.py
#   glibmm/tools/enum.pl
#   libnotify/libnotify/*.h
#   libnotify/libnotify/*.c
#   libnotifymm/tools/extra_defs_gen/generate_extra_defs

# Generated files:
#   libnotifymm/libnotify/src/libnotify_docs.xml
#   libnotifymm/libnotify/src/libnotify_enum.defs
#   libnotifymm/libnotify/src/libnotify_methods.defs
#   libnotifymm/libnotify/src/libnotify_signals.defs

GLIBMM_TOOLS_DIR=../../glibmm/tools
LIBNOTIFY_DIR=../../libnotify
LIBNOTIFYMM_LIBNOTIFY_SRC_DIR=../libnotify/src

$GLIBMM_TOOLS_DIR/defs_gen/docextract_to_xml.py \
  -s $LIBNOTIFY_DIR/libnotify \
  >$LIBNOTIFYMM_LIBNOTIFY_SRC_DIR/libnotify_docs.xml

$GLIBMM_TOOLS_DIR/enum.pl \
  $LIBNOTIFY_DIR/libnotify/*.h \
  >$LIBNOTIFYMM_LIBNOTIFY_SRC_DIR/libnotify_enum.defs

$GLIBMM_TOOLS_DIR/defs_gen/h2def.py \
  $LIBNOTIFY_DIR/libnotify/*.h \
  >$LIBNOTIFYMM_LIBNOTIFY_SRC_DIR/libnotify_methods.defs

extra_defs_gen/generate_extra_defs \
  >$LIBNOTIFYMM_LIBNOTIFY_SRC_DIR/libnotify_signals.defs

