/* main.cc
 *
 * Copyright (C) 2007 libnotifymm Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libnotifymm.h>
#include <iostream>

int main(int, char**)
{
  Notify::init("Basics");

  Notify::Notification n("Summary", 
                         "Content that is very long 8374983278r32j4 rhjjfh dw8f"
                         "43jhf 8ds7 ur2389f jdbjkt h8924yf jkdbjkt 892hjfiHER98HEJIF"
                         " BDSJHF hjdhF JKLH 890YRHEJHFU 89HRJKSHdd dddd ddddd dddd"
                         " ddddd dddd ddddd dddd dddd ddd ddd dddd Fdd d ddddd dddddddd"
                         " ddddddddhjkewdkjsjfjk sdhkjf hdkj dadasdadsa adsd asd sd"
                         " saasd fadskfkhsjf hsdkhfkshfjkhsd kjfhsjdkhfj ksdhfkjshkjfsd"
                         " sadhfjkhaskd jfhsdajkfhkjs dhfkjsdhfkjs adhjkfhasdkj"
                         " fhdsakjhfjk asdhkjkfhd akfjshjfsk afhjkasdhf jkhsdaj hf"
                         " kjsdfahkfh sakjhfksdah kfdashkjf ksdahfj shdjdh");
  n.set_timeout(3000); //3 seconds

  try
  {  
    if (!n.show()) 
    {
      std::cerr << "failed to send notification" << std::endl;
      return 1;
    }
  }
  catch (Glib::Error& err)
  {
    std::cout << err.what() << std::endl;
  }
  return 0;
}
