/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * libnotifymm
 * Copyright (C) Johannes Schmid 2007 <jhs@gnome.org>
 * 
 * libnotifymm is free software.
 * 
 * You may redistribute it and/or modify it under the terms of the
 * GNU General Public License, as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * libnotifymm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libnotifymm.  If not, write to:
 *   The Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor
 *   Boston, MA  02110-1301, USA.
 */

#ifndef _TEST_WINDOW_H_
#define _TEST_WINDOW_H_

#include <gtkmm.h>
#include <libnotifymm.h>

class TestWindow: public Gtk::Window
{
public:
  TestWindow();

protected:
  void on_button_clicked();
  void on_help(const Glib::ustring& action);
  void on_ignore(const Glib::ustring& action);
  void on_empty(const Glib::ustring& action);
  void on_closed();

  bool m_is_closed;
  Notify::Notification m_notify;
  Gtk::Button m_button;
};

#endif // _TEST_WINDOW_H_
