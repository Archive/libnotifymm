/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * libnotifymm
 * Copyright (C) Johannes Schmid 2007 <jhs@gnome.org>
 * 
 * libnotifymm is free software.
 * 
 * You may redistribute it and/or modify it under the terms of the
 * GNU General Public License, as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * libnotifymm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libnotifymm.  If not, write to:
 *   The Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor
 *   Boston, MA  02110-1301, USA.
 */

#include "test-window.h"
#include <iostream>

TestWindow::TestWindow() :
  m_is_closed(true),
  m_notify("Low disk space", "You can free up some disk space by "
           "emptying the trash can."),
  m_button("Test")
{
  set_default_size(150, 80);

  m_notify.set_timeout(10000); // 10 seconds
  m_notify.set_urgency(Notify::URGENCY_CRITICAL);

  m_notify.add_action("help", "Help", sigc::mem_fun(*this, &TestWindow::on_help));
  m_notify.add_action("ignore", "Ignore", sigc::mem_fun(*this, &TestWindow::on_ignore));
  m_notify.add_action("empty", "Empty Trash", sigc::mem_fun(*this, &TestWindow::on_empty));

  m_notify.set_category("device");

  m_notify.signal_closed().connect(sigc::mem_fun(*this, &TestWindow::on_closed));
  m_button.signal_clicked().connect(sigc::mem_fun(*this, &TestWindow::on_button_clicked));

  add(m_button);
  show_all_children();
}

void TestWindow::on_button_clicked()
{
  try
  {
    if (!m_is_closed)
    {
      m_notify.close();
      m_is_closed = true;
    }
    if (m_notify.show())
    {
      m_is_closed = false;
    }
    else
    {
      std::cerr << "failed to send notification" << std::endl;
    }
  }
  catch (Glib::Error& err)
  {
    std::cout << err.what() << std::endl;
  }
}

void TestWindow::on_help(const Glib::ustring& action)
{
  std::cout << "You clicked help: " << action << std::endl;
}

void TestWindow::on_ignore(const Glib::ustring& action)
{
  std::cout << "You clicked ignore: " << action << std::endl;  
}

void TestWindow::on_empty(const Glib::ustring& action)
{
  std::cout << "You clicked empty: " << action << std::endl;  
}

void TestWindow::on_closed()
{
  m_is_closed = true;
  std::cout << "Notification closed" << std::endl;  
}

