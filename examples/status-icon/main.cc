/* main.cc
 *
 * Copyright (C) 2007 libnotifymm Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include <libnotifymm.h>
#include <gtkmm.h>
#include <iostream>


int main (int argc, char** argv)
{
  Notify::init("Basics");
  Gtk::Main kit(argc, argv);
  
  Glib::RefPtr<Gtk::StatusIcon> status_icon =
    Gtk::StatusIcon::create(Gtk::Stock::INFO);
  Notify::Notification n("StatusIcon", 
                         "Testing StatusIcon mode with low urgency",
                         "gtk-info", status_icon);
  n.set_timeout(3000); //3 seconds
  n.set_urgency(Notify::URGENCY_LOW);

#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {  
  if (!n.show()) 
#else
  std::auto_ptr<Glib::Error> error;
  if (!n.show(error))
#endif
  {
    std::cerr << "failed to send notification" << std::endl;
    return 1;
  }
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  }
  catch (Glib::Error& err)
  {
    std::cout << err.what() << std::endl;
  }
#endif
  
  kit.run();
  
  return 0;
}


