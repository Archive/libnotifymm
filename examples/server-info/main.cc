/* main.cc
 *
 * Copyright (C) 2007 libnotifymm Development Team
 *
 * This program is free software; you can redistribute it and/or 
 * modify it under the terms of the GNU General Public License as 
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libnotifymm.h>
#include <gtkmm.h>
#include <iostream>

int main(int, char**)
{
  Notify::init("Basics");
  Glib::ustring name, vendor, version, spec_version;

  if (!Notify::get_server_info(name, vendor, version, spec_version))
  {
    std::cerr << "Failed to receive server info.\n" << std::endl;
    exit(1);
  }

  std::cout << "Name: " << name << std::endl;
  std::cout << "Vendor: " << vendor << std::endl;
  std::cout << "Version: " << version << std::endl;
  std::cout << "Spec version: " << spec_version << std::endl;
  
  std::cout << "Capabilites:" << std::endl;
  Glib::ListHandle<Glib::ustring> caps = Notify::get_server_caps();

  if (!caps.size())
  {
    std::cerr << "Failed to receive server caps.\n" << std::endl;
    exit(1);
  }

  for (Glib::ListHandle<Glib::ustring>::const_iterator ci = caps.begin(); ci != caps.end(); ci++)
  {
    std::cout << "\t" << *ci << std::endl;
  }

  return 0;
}
