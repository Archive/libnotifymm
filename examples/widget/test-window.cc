/* -*- Mode: C; indent-tabs-mode: t; c-basic-offset: 4; tab-width: 4 -*- */
/*
 * libnotifymm
 * Copyright (C) Johannes Schmid 2007 <jhs@gnome.org>
 * 
 * libnotifymm is free software.
 * 
 * You may redistribute it and/or modify it under the terms of the
 * GNU General Public License, as published by the Free Software
 * Foundation; either version 2 of the License, or (at your option)
 * any later version.
 * 
 * libnotifymm is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with libnotifymm.  If not, write to:
 *   The Free Software Foundation, Inc.,
 *   51 Franklin Street, Fifth Floor
 *   Boston, MA  02110-1301, USA.
 */

#include "test-window.h"
#include <iostream>

TestWindow::TestWindow() :
  m_button("click here to change notification"),
  m_notify("Widget Attachment Test", "Button has not been clicked yet")
{
  m_notify.attach_to_widget(m_button);
  m_notify.set_timeout(0);
  
  m_button.signal_clicked().connect(sigc::mem_fun(*this, &TestWindow::on_clicked));
  m_button.signal_expose_event().connect(sigc::mem_fun(*this, &TestWindow::on_exposed));
  
  add(m_button);
  show_all_children();
}

bool TestWindow::on_exposed (GdkEventExpose* ev)
{
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {  
  if (!m_notify.show()) 
#else
  std::auto_ptr<Glib::Error> error;
  if (!m_notify.show(error))
#endif
  {
    std::cerr << "failed to send notification" << std::endl;
  }
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  }
  catch (Glib::Error& err)
  {
    std::cout << err.what() << std::endl;
  }
#endif
  
  return false;
}

void TestWindow::on_clicked()
{
  static int count = 0;
  count++;
  
  gchar* buf = g_strdup_printf ("You clicked the button %i times", count);
    
  m_notify.update ("Widget Attachment Test", buf, "");
  g_free(buf);
  
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  try
  {  
  if (!m_notify.show()) 
#else
  std::auto_ptr<Glib::Error> error;
  if (!m_notify.show(error))
#endif
  {
    std::cerr << "failed to send notification" << std::endl;
  }
#ifdef GLIBMM_EXCEPTIONS_ENABLED
  }
  catch (Glib::Error& err)
  {
    std::cout << err.what() << std::endl;
  }
#endif
}
